<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
	<meta charset="UTF-8">
	<title title="Book Management">Book Managament</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
	body{
       background-image: linear-gradient(to right, red,yellow);
    }
    .header{
        background-color: black;
        color: white;
        height: 100px;
        margin-top: -7px;
        margin-left: -15px;
        width: 101%;
    }
    .title{
        font-family: Arial, Helvetica, sans-serif;
        display: inline-block;
        text-align: center;
        margin-top: 15px;
        font-size: 40px;
        margin-left: 15px;
        height: 50%;
        padding: 5px;
    }
    .navLink{
        display: inline-block;
        margin-left: 60%;
    }
    .navUL{
        list-style-type: none;
    }
    li{
        display: inline;
    }
    a{
        color: white;
        text-decoration: none;
    }
    .li{
        margin-left: 10px;
    }
    .mark{
    	font-size: 40px;
    	padding: 5px;
    	margin: 10px;
    	background-image: linear-gradient(to right,yellow,purple);
    	border-radius:10px; 
    	color: white;
    }
</style>
</head>
<body>
    <div class="header" id="headerId">
        <div class="title">Bookess</div>
        <div class="navLink">
            <ul class="navUL">
                <li class="li"><a href="login"><button class="btn btn-success">Login</button></a></li>
            	<li class="li"><a href="register" class="login"><button class="btn btn-primary">Register</button></a></li>
                <li class="li"><a href="book" class="login"><button class="btn btn-primary">Show All Books</button></a></li>
            </ul>
        </div>
    </div>
    
    <marquee class="mark" scrollamount="15"> Welcome to Bookess => Please Login to Continue</marquee>

</body>
</html>
