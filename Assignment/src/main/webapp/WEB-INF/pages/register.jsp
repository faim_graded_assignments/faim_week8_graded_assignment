<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<style>
    body{
       background-image: linear-gradient(to right, red,yellow);
    }
    .header{
        background-color: black;
        color: white;
        height: 100px;
        margin-top: -7px;
        margin-left: -15px;
        width: 101%;
    }
    .title{
        font-family: Arial, Helvetica, sans-serif;
        display: inline-block;
        text-align: center;
        margin-top: 15px;
        font-size: 40px;
        margin-left: 15px;
        height: 50%;
        padding: 5px;
    }
    .navLink{
        display: inline-block;
        margin-left: 70%;
    }
    .navUL{
        list-style-type: none;
    }
    li{
        display: inline;
    }
    a{
        color: white;
        text-decoration: none;
    }
    .li{
        margin-left: 10px;
    }

    .cardLogin{
        margin-left: 30%;
        margin-top: 20px;
        border: 2px solid black;
        display: inline-block;
        width: 35%;
        height: 450px;
        border-radius: 10px;
    }
    .loginTitle {
            background-image: linear-gradient(to right, rgb(107, 193, 214), rgb(223, 223, 50));
            text-align: center;
            font-size: 30px;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
            height: 90px;
            font-size: 50px;
            padding: 5px;
            color: black;
        }
    .userName{
        margin: 10px;
        padding: 5px;
        font-family: Arial, Helvetica, sans-serif;
    }
    .inp{
        background-image: linear-gradient(to right,rgb(191, 231, 181),rgb(230, 230, 55));
        height: 50px;
        color: black
    }
    .mark{
    	font-size: 40px;
    	padding: 5px;
    	margin: 10px;
    	background-image: linear-gradient(to right,yellow,purple);
    	border-radius:10px; 
    	color: white;
    }
     .mark{
    	font-size: 40px;
    	padding: 5px;
    	margin: 10px;
    	background-image: linear-gradient(to right,yellow,purple);
    	border-radius:10px; 
    	color: white;
    }
    .cardLogin:hover {
		transform: scale(1.10);
		cursor: pointer;
	}
    </style>
<body>
	<div class="header" id="headerId">
        <div class="title">Bookess</div>
        <div class="navLink">
            <ul class="navUL">
            	<li class="li"><a href="login" class="login"><button class="btn btn-primary">Login</button></a></li>
                <li class="li"><a href="book" class="login"><button class="btn btn-primary">Show All Books</button></a></li>
            </ul>
        </div>
    </div>
    
    <marquee class="mark" scrollamount="15"> Welcome to Bookess => Please Register Yourself</marquee>
    
    <form action="getregistered" method="post" >
    <div class="cardLogin">
        <div class="loginTitle">Register</div>
        <div class="form-group userName">
            <label for="email">Email:</label>
            <input type="text" class="form-control inp" id="email" placeholder="Enter email" name="email">
        </div>

        <div class="form-group userName">
            <label for="name">Name:</label>
            <input type="text" class="form-control inp" id="name" placeholder="Enter Name" name="name">
        </div>

        <div class="form-group userName">
            <label for="password">Password:</label>
            <input type="password" class="form-control inp" id="email" placeholder="Enter Password" name="password">
        </div>

        <div align="center">
            <button type="submit" class="btn btn-success" value="Register">Register</button>
        </div>
    </div>
    
    </form>
</body>
</html>