<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title title="Book Management">Book Managament</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        body {
            background-image: linear-gradient(to right, red, yellow);
        }

        .header {
            background-color: black;
            color: white;
            height: 100px;
            margin-top: -7px;
            margin-left: -15px;
            width: 101%;
        }

        .title {
            font-family: Arial, Helvetica, sans-serif;
            display: inline-block;
            text-align: center;
            margin-top: 15px;
            font-size: 40px;
            margin-left: 15px;
            height: 50%;
            padding: 5px;
        }

        .navLink {
            display: inline-block;
            margin-left: 68%;
        }

        .navUL {
            list-style-type: none;
        }

        li {
            display: inline;
        }

        a {
            color: white;
            text-decoration: none;
        }

        .li {
            margin-left: 10px;
        }

        .cardLogin {
            margin-left: 30%;
            margin-top: 20px;
            border: 2px solid black;
            display: inline-block;
            width: 35%;
            height: 350px;
            border-radius: 10px;
        }

        .cardLogin:hover {
            transform: scale(1.10);
            cursor: pointer;
        }

        .loginTitle {
            background-image: linear-gradient(to right, rgb(107, 193, 214), rgb(223, 223, 50));
            text-align: center;
            font-size: 30px;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
            height: 90px;
            font-size: 50px;
            padding: 5px;
            color: black;
        }

        .userName {
            margin: 10px;
            padding: 5px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .inp {
            background-image: linear-gradient(to right, rgb(191, 231, 181), rgb(230, 230, 55));
            height: 50px;
            color: black
        }

        .uname {
            margin-left: 150px;
        }

        .mark {
            font-size: 40px;
            padding: 5px;
            margin: 10px;
            background-image: linear-gradient(to right, yellow, purple);
            border-radius: 10px;
            color: white;
        }
    </style>
</head>

<body>
    <div class="header" id="headerId">
        <div class="title">Bookess</div>
        <div class="navLink">
            <ul class="navUL">
                <li class="li"><a href="register" class="login"><button
                            class="btn btn-primary">Register</button></a></li>
                <li class="li"><a href="book" class="login"><button class="btn btn-primary">Show All Books</button></a></li>
            </ul>
        </div>
    </div>

    <marquee class="mark" scrollamount="15"> Welcome to Bookess => Please Login to Continue</marquee>

    <form action="dashboard" method="POST">
        <div class="cardLogin">
            <div class="loginTitle">Login</div>
            <div class="form-group userName">
                <label for="username">Email:</label>
                <input type="text" class="form-control inp" id="email" placeholder="Enter email" name="email">
            </div>
            <div class="form-group userName">
                <label for="password">Password:</label>
                <input type="password" class="form-control inp" id="email" placeholder="Enter Password" name="password">
            </div>

            <div align="center">
                <button type="submit" class="btn btn-success" value="Login">Login</button>
            </div>
        </div>
    </form>
</body>

</html>