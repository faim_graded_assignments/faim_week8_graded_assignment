<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<style>
body {
	background-image: linear-gradient(to right, red, yellow);
}

.header {
	background-color: black;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
	position: fixed;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

.navLink {
	display: inline-block;
	margin-left: 54%;
}

.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}

.cardforbook1 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin-left: 3%;
	height: 300px;
	float: left;
}

.cardforbook1:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.upperdiv {
	background-color: rgb(44, 92, 197);
	height: 20px;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	height: 70px;
	margin-left: -2px;
	margin-top: -2px;
	margin-right: -2px;
}

img {
	width: 90px;
	height: 90px;
	border-radius: 50%;
	margin-top: -45px;
}

.id {
	padding: 5px;
	text-align: center;
	margin: 5px;
	font-size: 18px;
	width: 100%;
	color: white;
	font-size: 40px;
}

hr {
	margin: 2px;
}

.btnLeft {
	width: 30%;
	float: left;
	margin-left: 15%;
	margin-top: 40px;
	height: 70px;
	font-size: 30px;
	margin-bottom: 50px;
}

.btnright {
	margin-left: 10%;
	width: 30%;
	margin-top: 40px;
	margin-right: 10%;
	height: 70px;
	font-size: 30px;
	margin-bottom: 50px;
}

.id2 {
	padding: 5px;
	text-align: right;
	margin: 5px;
	font-size: 18px;
	width: 40%;
	color: white;
}

.cardforbook2 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin-right: 3%;
	height: 300px;
	float: right;
}

.cardforbook2:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.cardforbook3 {
	border: 2px solid blue;
	border-radius: 10px;
	width: 400px;
	text-align: center;
	margin: auto;
	height: 300px;
}

.cardforbook3:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.wrap {
	margin-top: 30px;
	border: 2px solid black;
	padding: 5px;
	margin-left: 10px;
	margin-right: 10px;
	border-radius: 5px;
}

.data {
	font-size: 40px;
}

.num {
	margin-top: -5px;
	font-size: 55px;
	color: black;
	background-color: black;
	display: block;
	margin-bottom: 10px;
	margin-left: -5px;
	margin-right: -5px;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	height: 90px;
	text-align: center;
	color: white;
}

.mark {
	font-size: 40px;
	padding: 5px;
	margin-left: 10px;
	margin-right: 10px;
	margin-top: 110px;
	background-image: linear-gradient(to right, yellow, purple);
	border-radius: 10px;
	color: white;
}
.name{
    	color: black;
    	background-color: white;
    	border-radius: 5px;
    	padding: 3px;
    }
    .name:hover{
    	color: white;
    	background-color: black;
    	padding: 3px;
    	cursor: pointer;
    	text-decoration: underline;
</style>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");
	
	%>
	<div class="header" id="headerId">
		<div class="title">Bookess</div>
		<div class="navLink">
			<ul class="navUL">
				<li class="li"><a href="dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a></li>
				<li class="li"><a href="read" class="login"><button
							class="btn btn-primary">Read Later</button></a></li>
				<%--
				if (email != null) {
				--%>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%--
				} else{
				%>
				<li class="li"><a href="login.jsp" class="login"><button class="btn btn-primary">Login</button></a></li>
                <%}--%>
			</ul>
		</div>
	</div>
	<marquee class="mark" scrollamount="15">
		Welcome <strong class="name" title="Your Name"><%=name%></strong> to
		Bookess => Choose Option from the Navigation bar
	</marquee>
	<%
	int i = 0;
	%>
	<c:forEach items="${listOfAllReadLaterBooks}" var="bookList">
		<div class="wrap">
			<div class="num">
				Book
				<%=++i%></div>
			<div class="cardforbook1">
				<div class="upperdiv"></div>
				<div class="round">
					<img alt="error loading"
						src=" <c:url value="/assets/images/image1.png" />">
				</div>
				<div class="id">Book Id is: </div>
				<div class="data">${bookList.getBookId()}</div>
			</div>
			<div class="cardforbook2">
				<div class="upperdiv"></div>
				<div class="round">
					<img alt="error loading"
						src=" <c:url value="/assets/images/image1.png" />">
				</div>
				<div class="id">Book Name is:</div>
				<div class="data">${bookList.getBookName()}</div>
			</div>
			<div class="cardforbook3">
				<div class="upperdiv"></div>
				<div class="round">
					<img alt="error loading"
						src=" <c:url value="/assets/images/image1.png" />">
				</div>
				<div class="id">Book Genre is:</div>
				<div class="data">${bookList.getBookGenre()}</div>
			</div>
		</div>
	</c:forEach>
</body>
</html>