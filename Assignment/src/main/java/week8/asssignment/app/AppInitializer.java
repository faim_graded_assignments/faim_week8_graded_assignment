package week8.asssignment.app;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	public AppInitializer() {
		System.out.println("Week8 App Initializer is Loaded");
	}

    @Override
     protected Class<?>[] getRootConfigClasses() {
          return null;
     }

     @Override
     protected Class<?>[] getServletConfigClasses() {
    	 System.out.println("App Initializer");
         return new Class<?>[] { WebAppConfig.class };
     }

     @Override  
     protected String[] getServletMappings() {
          return new String[] { "/" };
     }


}
