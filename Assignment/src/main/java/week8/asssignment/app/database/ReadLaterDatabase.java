package week8.asssignment.app.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import week8.asssignment.app.model.ReadLater;

@Repository
public class ReadLaterDatabase {
	
	@Autowired
	private JdbcTemplate template;
	
	
	public boolean alreadyPresentInReadLater(int id) {
		System.out.println("in alredyPresent method of read later");
		String sql = "select * from readlater where bookId='"+id+"'";
		List<ReadLater> readLater = this.template.query(sql,new ReadLaterRowMapper());
		System.out.println(readLater);
		if(readLater.isEmpty()) {
			return false;
		}else {
			return true;
		}
	}
	
	public boolean insertIntoReadLater(ReadLater readlater,HttpSession session) throws Exception {
		
		String sql = "insert into readlater values(?,?,?,?)";
				
				try {
				int count = template.update(sql,readlater.getBookId(),readlater.getBookName(),
						readlater.getBookGenre(),session.getAttribute("email"));
				System.out.println("count of the radlater books " + count);
				}
				catch(DataAccessException e)
				{
					throw new Exception(e.getMessage());
				}
				return true;
	}
	
	public List<ReadLater> getAllReadLaterBooks(HttpSession session){

		String sql = "select * from liked where email='"+session.getAttribute("email")+"'";
		return this.template.query(sql,new ReadLaterRowMapper());
	}
	
	class ReadLaterRowMapper implements RowMapper<ReadLater> {

		@Override
		public ReadLater mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			ReadLater readLater = new ReadLater();
			
			readLater.setBookId(resultSet.getInt(1));
			readLater.setBookName(resultSet.getString(2));
			readLater.setBookGenre(resultSet.getString(3));
			
			return readLater;
		}
	}
	
	
}
