package week8.asssignment.app.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import week8.asssignment.app.model.Book;

@Repository
public class BookDatabase {

	@Autowired
	private JdbcTemplate template;

	public List<Book> getAllBooks(){

		String sql = "select * from book;";
		return this.template.query(sql, new BookRowMapper());
	}
	
	public Book getBookById(int id) {
		String sql = "select * from book where bookId=?";
		return (Book) this.template.queryForObject(sql, new Object[] {id},new BookRowMapper());
	}
	
	class BookRowMapper implements RowMapper<Book> {

		@Override
		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			Book book = new Book();

			book.setBookId(rs.getInt(1));
			book.setBookName(rs.getString(2));
			book.setBookGenre(rs.getString(3));
			
			return book;
		}
	}
}
