package week8.asssignment.app.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import week8.asssignment.app.model.Liked;

@Repository
public class LikedDatabase {
	
	@Autowired
	private JdbcTemplate template;
	
	
	public boolean alreadyPresentInLiked(int id) {
		System.out.println("in alredyPresent method");
		String sql = "select * from liked where bookId='"+id+"'";
		List<Liked> liked = this.template.query(sql,new LikedRowMapper());
		System.out.println(liked);
		if(liked.isEmpty()) {
			return false;
		}else {
			return true;
		}
	}
	
	public boolean insertIntoLiked(Liked liked,HttpSession session) throws Exception {
		
		String sql = "insert into liked values(?,?,?,?)";
				
				try {
				int count = template.update(sql,liked.getBookId(),liked.getBookName(),
						liked.getBookGenre(),session.getAttribute("email"));
				
				System.out.println("count od liked books" + count);
				}
				catch(DataAccessException e)
				{
					throw new Exception(e.getMessage());
				}
				return true;
	}
	
	public List<Liked> getAllLikedBooks(HttpSession session){

		String sql = "select * from liked where email='"+session.getAttribute("email")+"'";
		return this.template.query(sql,new LikedRowMapper());
	}
	
	class LikedRowMapper implements RowMapper<Liked> {

		@Override
		public Liked mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			Liked liked = new Liked();
			
			liked.setBookId(resultSet.getInt(1));
			liked.setBookName(resultSet.getString(2));
			liked.setBookGenre(resultSet.getString(3));
			
			return liked;
		}
	}
	
}
