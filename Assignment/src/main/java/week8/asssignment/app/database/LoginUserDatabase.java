package week8.asssignment.app.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import week8.asssignment.app.model.LoginUser;
import org.springframework.jdbc.core.RowMapper;

@Repository
public class LoginUserDatabase {


	@Autowired
	private JdbcTemplate template;

	public LoginUser getUserByEmail(String email) {

		String sql = "select email,name,password from loginuser where email=?";
		return (LoginUser)this.template.queryForObject(sql,new Object[] {email},new LoginUserRowMapper());
	}
	
	public List<LoginUser> getLoginUserByEmail(String email) {

		String sql = "select email,name,password from loginuser where email='"+email+"'";
		return this.template.query(sql,new LoginUserRowMapper());
	}

	public boolean insertIntoLoginUser(LoginUser user) throws Exception {

		String sql = "insert into loginuser values(?,?,?)";

		try {
			int count = template.update(sql,user.getEmail(),user.getName(),user.getPassword());
			System.out.println("count of loginuser books" + count);
		}
		catch(DataAccessException e)
		{
			throw new Exception(e.getMessage());
		}
		return true;
	}

	class LoginUserRowMapper implements RowMapper<LoginUser> {

		@Override
		public LoginUser mapRow(ResultSet rs, int rowNum) throws SQLException {
			System.out.println(rowNum);

			LoginUser user = new LoginUser();

			user.setEmail(rs.getString(1));
			user.setName(rs.getString(2));
			user.setPassword(rs.getString(3));

			return user;
		}
	}
}
