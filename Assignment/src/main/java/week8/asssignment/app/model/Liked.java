package week8.asssignment.app.model;

public class Liked {
	
	private int bookId;
	private String bookName;
	private String bookGenre;
	
	public Liked() {
		System.out.println("Liked Class Constructor");
	}

	public Liked(int bookId, String bookName, String bookGenre) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	@Override
	public String toString() {
		return "Liked [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + "]";
	}
	
}
