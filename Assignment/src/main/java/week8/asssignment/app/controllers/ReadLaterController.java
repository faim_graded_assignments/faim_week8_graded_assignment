package week8.asssignment.app.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import week8.asssignment.app.database.BookDatabase;
import week8.asssignment.app.database.ReadLaterDatabase;
import week8.asssignment.app.model.Book;
import week8.asssignment.app.model.ReadLater;

@Controller
public class ReadLaterController {
	
	@Autowired
	private ReadLaterDatabase readLaterDatabase;
	
	
	@Autowired
	private BookDatabase bookDatabase;

	@RequestMapping(value = "/addread/{id}",method = RequestMethod.GET)
	public String addToLiked(@PathVariable int id,HttpSession session) {

		System.out.println("id of the book that is readlater "+id);

		boolean isInserted = false;
		boolean checkInReadLater = false;
		Book readLaterBook = bookDatabase.getBookById(id);
		System.out.println(readLaterBook);
		if(readLaterBook != null) {
			ReadLater readLater = new ReadLater(readLaterBook.getBookId(),readLaterBook.getBookName(),readLaterBook.getBookGenre());
			try {
				checkInReadLater = readLaterDatabase.alreadyPresentInReadLater(id);
				if(!checkInReadLater) {
					isInserted = readLaterDatabase.insertIntoReadLater(readLater,session);
					System.out.println("Values inserted into readLater");
				}else {
					return "redirect:error";
				}
				
			} catch (Exception e) {
				session.setAttribute("error","Error while Connectinf SQL Database");
				return "redirect:error";
			}
		}
		if(isInserted) {
			return "dashboard";
		}else {
			return "redirect:error";
		}
	}
	@GetMapping("/errorread")
	public String errorMessage() {

		return "error";
	}

	@GetMapping("/read")
	public String getReadLaterBooks(Map<String,List<ReadLater>> map,HttpSession session) {
		System.out.println("ReadLater getReadLaterBooks() is called");
		List<ReadLater> listOfAllReadLaterBooks = readLaterDatabase.getAllReadLaterBooks(session);

		if(!listOfAllReadLaterBooks.isEmpty()) {
			map.put("listOfAllReadLaterBooks",listOfAllReadLaterBooks);
			for(ReadLater readLater : listOfAllReadLaterBooks) {
				System.out.println(readLater.getBookId());
				System.out.println(readLater.getBookName());
				System.out.println(readLater.getBookGenre());
			}
			return "liked";
		}else {
			session.setAttribute("error","You do not have any Read Later Book");
			return "redirect:error";
		}
	}

	@GetMapping("/readtodashboard")
	public String getDashboard(HttpSession session) {
		System.out.println("Liked get called");
		if(session.getAttribute("email") != null) {
			return "dashboard";
		}else {
			return "redirect:login";
		}
	}
	
}
