package week8.asssignment.app.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import week8.asssignment.app.database.BookDatabase;
import week8.asssignment.app.database.LikedDatabase;
import week8.asssignment.app.model.Book;
import week8.asssignment.app.model.Liked;

@Controller
public class LikedController {

	@Autowired
	private LikedDatabase likedDatabase;

	@Autowired
	private BookDatabase bookDatabase;

	@RequestMapping(value = "/addliked/{id}",method = RequestMethod.GET)
	public String addToLiked(@PathVariable int id,HttpSession session) {

		System.out.println("id of the book that is liked "+id);

		boolean isInserted = false;
		boolean checkInLiked = false;
		Book likedBook = bookDatabase.getBookById(id);
		System.out.println(likedBook);
		if(likedBook != null) {
			Liked liked = new Liked(likedBook.getBookId(),likedBook.getBookName(),likedBook.getBookGenre());
			try {
				checkInLiked = likedDatabase.alreadyPresentInLiked(id);
				if(!checkInLiked) {
					isInserted = likedDatabase.insertIntoLiked(liked,session);
					System.out.println("Values inserted into liked");
				}else {
					return "redirect:error";
				}
				
			} catch (Exception e) {
				session.setAttribute("error","SQL Connection Error");
				return "redirect:error";
			}
		}
		if(isInserted) {
			return "dashboard";
		}else {
			return "redirect:error";
		}
	}
	@GetMapping("/error")
	public String errorMessage() {
		System.out.println("page for All extra Activity");
		return "error";
	}

	@GetMapping("/liked")
	public String getLikedBooks(Map<String,List<Liked>> map,HttpSession session) {
		System.out.println("Liked getLikedBooks() is called");
		List<Liked> listOfAllLikedBooks = likedDatabase.getAllLikedBooks(session);

		if(!listOfAllLikedBooks.isEmpty()) {
			
			map.put("listOfAllLikedBooks",listOfAllLikedBooks);
			
			for(Liked liked : listOfAllLikedBooks) {
				System.out.println(liked.getBookId());
				System.out.println(liked.getBookName());
				System.out.println(liked.getBookGenre());
			}
			return "liked";
		}else {
			session.setAttribute("error","You do not have any Liked Book");
			return "redirect:error";
		}
		
	}

	@GetMapping("/dashboard")
	public String getDashboard(HttpSession session) {
		System.out.println("Liked get called");
		if(session.getAttribute("email") != null) {
			return "dashboard";
		}else {
			return "redirect:login";
		}
	}
}
