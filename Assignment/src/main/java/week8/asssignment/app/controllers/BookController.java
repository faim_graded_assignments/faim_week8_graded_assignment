package week8.asssignment.app.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import week8.asssignment.app.database.BookDatabase;
import week8.asssignment.app.model.Book;

@Controller
public class BookController {

	@Autowired
	private BookDatabase bookDatabase;

	public BookController() {
		System.out.println("Book Controller");
	}

	@GetMapping("/book")
	public String getBooks(Map<String,List<Book>> map,HttpSession session) {
		System.out.println("Book getBooks() is called");
		List<Book> listOfAllBooks = bookDatabase.getAllBooks();

		map.put("listOfAllBooks",listOfAllBooks);
		for(Book b : listOfAllBooks) {
			System.out.println(b.getBookId());
			System.out.println(b.getBookName());
			System.out.println(b.getBookGenre());
		}
		return "book";
	}
}
