package week8.asssignment.app.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import week8.asssignment.app.database.LoginUserDatabase;
import week8.asssignment.app.model.LoginUser;

@Controller
public class LoginUserController {

	@Autowired
	private LoginUserDatabase loginUserDatabase;

	@PostMapping("/dashboard")
	public String LoginUser(LoginUser loginUser,HttpSession session) {
		System.out.println("Book loginUser(LoginUser loginUser) is called");

		System.out.println(loginUser.getEmail());

		LoginUser loggedInUser = loginUserDatabase.getUserByEmail(loginUser.getEmail());

		System.out.println(loggedInUser.getEmail());

		if(!loggedInUser.getEmail().equals(loginUser.getEmail())) {

			System.out.println("getUserByEmail returning null value");
			System.out.println(loggedInUser);

			return "redirect:register";
		}else if(loggedInUser.getPassword().equals(loginUser.getPassword())){

			System.out.println("not returning null values");

			session.setAttribute("email",loginUser.getEmail());
			session.setAttribute("name",loggedInUser.getName());

			return "dashboard";
		}else {
			System.out.println("none of the conditions worked");
			session.setAttribute("error","You are not Registered");
			return "redirect:error";
		}
	}

	@GetMapping("/login")
	public String login(HttpSession session) {
		System.out.println("Book login() is called");
		return "login";
	}

	@GetMapping("/register")
	public String registerToDatabase() {
		System.out.println("Book registerToDatabase() is called");
		return "register";
	}

	@PostMapping("/getregistered")
	public String register(LoginUser user,HttpSession session) {
		System.out.println("Book register() is called");

		boolean isInserted = false;

		List<LoginUser> loginUser = loginUserDatabase.getLoginUserByEmail(user.getEmail());
		LoginUser newUser = new LoginUser(user.getEmail(),user.getName(),user.getPassword());

		System.out.println(loginUser);
		try {
			if(!loginUser.isEmpty()) {
				System.out.println("not null");
				session.setAttribute("error","User AlreadyExists Please Login");
				return "redirect:error";
			}else {
				System.out.println("not null value from db");
				isInserted = loginUserDatabase.insertIntoLoginUser(newUser);
			}
		} catch (Exception e) {
			session.setAttribute("error","SQL Connection Error");
			return "redirect:error";
		}
		if(isInserted) {
			return "login";
		}else {
			session.setAttribute("error","user with "+user.getEmail()+" not Inserted due to SQl Error");
			return "redirect:error";
		}

	}

	@GetMapping("/logout")
	public String logout(HttpSession session) {
		System.out.println("Book logout() is called");

		session.removeAttribute("email");
		session.removeAttribute("name");
		session.invalidate();

		return "redirect:login";
	}
}
